'use strict';
const axios = require('axios')
const translator = require('../../utils/translator')
const languages = require('../../utils/languages')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  async createTranslations(result, data, iteration = 0) {
    await languages.getLanguages()
    const langs = languages.languages.filter(
      (item) => item.locale !== 'ru-RU'
    );
    const lang = langs[iteration]

    const notification = {...data}

    let translations = await translator.translateText({
      text: [notification.title, notification.body],
      target: lang.short
    })

    try {
      await axios
        .post(`http://${process.env.HOST}:${process.env.PORT}/general-notifications/${result.id}/localizations`,
          {
            ...notification,
            title: translations[0],
            body: translations[1],
            locale: lang.locale
          })
    } catch (e) {
      throw e
    }

    if (iteration !== langs.length - 1) {
      return this.createTranslations(result, data, (iteration += 1));
    } else {
      return null;
    }
  },

  deleteTranslations (params) {
    try {
      params.localizations.forEach((item) => {
        axios.delete(`http://${process.env.HOST}:${process.env.PORT}/general-notifications/${item._id}`)
      });
    } catch (e) {
      throw e;
    }
  }
};
