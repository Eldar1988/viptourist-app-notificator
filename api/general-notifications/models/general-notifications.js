'use strict';
const services = require('../services/general-notifications')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate (result, data) {
      if (!result.localizations.length) {
        await services.createTranslations(result, data)
      }
    },

    async afterDelete(params, result) {
      services.deleteTranslations(params)
    },
  }

};
