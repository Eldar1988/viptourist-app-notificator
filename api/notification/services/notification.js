'use strict';
const PushNotificator = require('../../../utils/push-notificator')
const nodemailer = require('nodemailer')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  async sendToProfile (result) {
    // Если нет профиля то пуш отравлять некому
    if (!result.profile) return console.log('Profile for push not found...')

    const notificator = new PushNotificator({
      title: result.title,
      body: result.body,
      image: result.image,
      link: result.link,
      fcm_token: result.profile.fcm_token
    })
    notificator.send()

    let transporter = await nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: 'noreply.viptourist@gmail.com',
        pass: 'ufnzwpudzlorrhki',
      },
    })

    try {
      transporter.sendMail({
        from: '"VIPTourist" <noreply.viptourist@gmail.com>',
        to: result.profile.email,
        subject: result.title,
        text: result.body
      })
    } catch (e) {
      console.log(e)
    }


  }
};
