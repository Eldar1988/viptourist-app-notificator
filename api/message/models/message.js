"use strict";
const services = require("../services/message");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate(result, data) {
      if (!result.localizations.length) {
        await services.createTranslations(result, data);
        await services.sendToProfiles(result, data);
      }
    },

    async beforeUpdate(params, data) {
      const message = await strapi.services.message.findOne({ id: params._id });
      if (!message.translationApproved && !!data.translationApproved) {
        await services.sendToProfiles(data, data);
      }
    },

    async afterDelete(params, result) {
      try {
        params.localizations.forEach((item) => {
          strapi.services.message.delete({ id: item._id });
        });
      } catch (e) {
        throw e;
      }
    },
  },
};
