"use strict";
const axios = require("axios");
const translator = require("../../utils/translator");
const languages = require("../../utils/languages");
const PushNotificator = require("../../../utils/push-notificator");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  // Create translations
  async createTranslations(result, data, iteration = 0) {
    await languages.getLanguages();
    const langs = languages.languages.filter((item) => item.locale !== "ru-RU");
    const lang = langs[iteration];

    const message = { ...data };

    let translations = await translator.translateText({
      text: [message.title, message.body],
      target: lang.short,
    });

    try {
      await axios.post(
        `http://${process.env.HOST}:${process.env.PORT}/messages/${result.id}/localizations`,
        {
          ...message,
          title: translations[0],
          body: translations[1],
          locale: lang.locale,
        }
      );
    } catch (e) {
      throw e;
    }

    if (iteration !== langs.length - 1) {
      return this.createTranslations(result, data, (iteration += 1));
    } else {
      return null;
    }
  },

  // ** Create translations

  // Send to profiles
  async sendToProfiles(result, data) {
    const tokens = await getProfilesTokens({
      forGuides: result.forGuides,
      forTourists: result.forTourists,
      locale: result.locale,
    });
    await sendToProfiles(result, data, tokens).then(() => {});
  },
};

async function getProfilesTokens({ forTourists, forGuides, locale }) {
  let profiles = [];
  const touristMessage = forTourists && !forGuides;
  const guidMessage = forGuides && !forTourists;
  const allMessage = forTourists && forGuides;
  const query = { locale };

  if (touristMessage) {
    query.is_tourist = true;
  } else if (guidMessage) {
    query.is_tourist = false;
  }

  profiles = await strapi.services.profile.find({ ...query });
  return profiles.map((item) => item.fcm_token);
}

async function sendToProfiles(result, data, tokens = []) {
  const notificator = new PushNotificator({
    title: result.title,
    body: result.body,
    image: result.image,
    link: result.link,
    fcm_tokens: tokens,
  });
  if (tokens.length) await notificator.sendMany();
}
