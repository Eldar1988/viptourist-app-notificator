const {firebaseAdminKey} = require('../config/keys')
const errorHandler = require('./error-handler')
const admin = require('firebase-admin')


module.exports = class PushNotificator {
  firebaseInit = false
  apiURL = 'https://fcm.googleapis.com/v1/projects/vip-tourist-c24cc/messages:send'
  notification = {
    title: '',
    body: ''
  }
  fcm_token = ''
  fcm_tokens = []
  link = ''

  constructor(notification) {
    this.notification.title = notification.title
    this.notification.body = notification.body
    this.fcm_token = notification.fcm_token
    this.fcm_tokens = notification.fcm_tokens
    this.image = notification.image
    this.link = notification.link

    this.init()
  }

  init() {

    if (!admin.apps.length) {
      admin.initializeApp({
        credential: admin.credential.cert(firebaseAdminKey),
      });
      this.firebaseInit = true
    }
  }

  async send() {
    const message = {
      notification: {
        title: this.notification.title,
        body: this.notification.body,
        image: this.image || ''
      },
      data: {
        link: this.link || ''
      },
      token: this.fcm_token
    }

    try {
      admin.messaging().send(message)
        .then((response) => {
          // Response is a message ID string.
          console.log('Successfully sent message');
        })
    } catch (err) {
      errorHandler(err)
    }
  }

  async sendMany() {
    const message = {
      notification: {
        title: this.notification.title,
        body: this.notification.body,
        image: this.image || ''
      },
      data: {
        link: this.link || ''
      },
      tokens: this.fcm_tokens
    }

    try {
      admin.messaging().sendMulticast(message)
        .then((response) => {
          // Response is a message ID string.
          console.log('Successfully sent all messages');
        })
    } catch (err) {
      errorHandler(err)
    }
  }
}
